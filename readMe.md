#Weather Forecast Program

Simple Java application which takes latitude and longitude as parameters to fetch weather forecast for a city
Ex: For Sunnyvale, CA coordinates are 37.3688° N, 122.0363° W.</n> 
Input to the application will be <b>latitude: 37.3688 </b>& <b>longitude: -122.0363 </b>

#References
1. Have used JSONPath Online Evaluator to help with Json parsing
http://jsonpath.com/


#Test scenario's
1. Happy path - Correct values as coordinates.
2. Pass empty parameters/inputs to the program
3. Passing negative values as inputs.
4. Passing alpha-numerics.
5. Values or strings exceeding the limit
6. Pass only Special characters (Ex: * or ! or #)
7. Reverse the order of lat and long (Basically incorrect values being passed)
8. Pass only latitude with empty longitude values.
9. Pass only longitude with empty latitude values.
10.Pass 0 as input to both the fields.