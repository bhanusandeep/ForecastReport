import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import com.jayway.jsonpath.JsonPath;
import org.json.simple.parser.ParseException;

public class getForecast {

	private static StringBuffer gridRsp = new StringBuffer();
	private static StringBuffer forecastRsp = new StringBuffer();

	
	private static String foreCastData,gridURL,pointsAPI, latCoord, longCoord ;
	private static String pointsAPIURL = "https://api.weather.gov/points/";
	private static Scanner sc= new Scanner(System.in);
	
	
	public static void main(String[] args) throws IOException, ParseException {
		
		
		System.out.println("Enter latitude, longitude coordinates of your city to retrieve weather forecast for the next 5 days");
		
		//Accepting user inputs
		System.out.println("Please enter latitude coordinate:");
		latCoord = sc.nextLine();
		
		System.out.println("Please enter longitude coordinate:");
		longCoord = sc.nextLine();
		sc.close();
		
		if(!(latCoord.isEmpty() ||longCoord.isEmpty())) {
		
		//Preparing final endpoint for points API
		pointsAPI = pointsAPIURL+latCoord+","+longCoord;
		
		
		//Retrieve GridPoints for the coordinates shared using pointsAPI
		gridRsp = fetchResponseData(pointsAPI);
		if(!(gridRsp.toString().isEmpty())) {
		gridURL = JsonPath.read(gridRsp.toString(),"$.properties.forecast");
		
		System.out.println("**** Weather Forecast details for city: "+JsonPath.read(gridRsp.toString(),"$.properties.relativeLocation.properties.city")+","+JsonPath.read(gridRsp.toString(),"$.properties.relativeLocation.properties.state")+"****");

		//Invoke foreCast API using the GridPoints
		forecastRsp = fetchResponseData(gridURL);
		
		System.out.println("----\t\t\t----------------");
		System.out.println("Day\t\t\tDetailedForeCast");
		System.out.println("----\t\t\t----------------");
		
		for(int i=0;i<12;i++) {
		System.out.print(""+JsonPath.read(forecastRsp.toString(),"$.properties.periods["+i+"].name"));
		
		System.out.println("\t\t"+JsonPath.read(forecastRsp.toString(),"$.properties.periods["+i+"].detailedForecast"));
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		}
	  }	
	 }	
		else
		System.out.println("***Latitude, Longitude coordinates are mandatory & cannot be empty, please enter valid inputs and re-run the application***");
	}
	
	
	
	//Method to read json data from the response
		private static StringBuffer getApiResponse(HttpsURLConnection forecastConnection){
		    
			StringBuffer response = new StringBuffer();
			
		    try{
		        BufferedReader in = new BufferedReader(new InputStreamReader(forecastConnection.getInputStream()));
		        while((foreCastData = in.readLine()) != null){
		            response.append(foreCastData);
		        }
		        in.close();
		    }catch(Exception e){
		        
		        
		    }
		    return response;
		}
		
		
	//Method to trigger the endpoint and fetch response from api	
		private static StringBuffer fetchResponseData(String newurl) throws IOException{
			
			//StringBuffer rsp1 = new StringBuffer();
			URL url = new URL(newurl);
			
			HttpsURLConnection forecastConnection = (HttpsURLConnection)url.openConnection();
			forecastConnection.setRequestMethod("GET");
			forecastConnection.setRequestProperty("Content-Type", "application/json");
			
			if(forecastConnection.getResponseCode()!=200) {
				System.out.println("\n****Please enter valid inputs for lat & long coordinates for fetch weather forecast****");
				System.out.println("\nSample user input - latitude:37.3688 and longitude:-122.0363");
				
				System.out.println("Current user input - latitude:"+latCoord+" and longitude:"+longCoord);
				

			}
			
			StringBuffer rsp1 = getApiResponse(forecastConnection);
			
			
			return rsp1;
		}
}
